# Repositories
> ## Python Repository
> - Artificial Intelligent Traveling Salesman https://gitlab.com/chinyan.demo/lrt-and-mrt-traveling-salesman-problem
> - Backend FastAPI Framework https://gitlab.com/chinyan.demo/pythonfastapi

> ## Go Repository
>- Gin Framework API  https://gitlab.com/chinyan.demo/golangapi

> ## Nodejs Repository
> - Koa Typescript API https://gitlab.com/chinyan.demo/nodejstypescriptapi
> - React Typescript Web Application https://gitlab.com/chinyan.demo/nodejstypescriptreactweb